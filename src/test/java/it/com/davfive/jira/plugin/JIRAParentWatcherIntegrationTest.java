/* Copyright (c) 2008, 2009, Ray Barham
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Ray Barham ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ray Barham BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package it.com.davfive.jira.plugin;

import static it.com.davfive.jira.IntegrationTestHelper.COMPONENT_ART_ID;
import static it.com.davfive.jira.IntegrationTestHelper.COMPONENT_CODE_ID;
import static it.com.davfive.jira.IntegrationTestHelper.COMPONENT_OTHER_ID;
import static it.com.davfive.jira.IntegrationTestHelper.Parent_WatcherS_LINK;
import static it.com.davfive.jira.IntegrationTestHelper.PLUGIN_KEY;
import static it.com.davfive.jira.IntegrationTestHelper.PROJECT_NAME;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.BindException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import org.xml.sax.SAXException;

import com.atlassian.jira.webtests.EmailFuncTestCase;
import com.atlassian.jira.webtests.JIRAServerSetup;
import com.meterware.httpunit.WebForm;

/**
 * Integration test for the JIRA Parent Watcher Plugin.
 * 
 * TODO: Add test for adding user watchers in the group area and group watchers in the user area
 * 
 * @author Ray Barham
 */
public class JIRAParentWatcherIntegrationTest extends EmailFuncTestCase {
    @Override
	protected void configureAndStartSmtpServer() {
    	try {
			mailService.configureAndStartGreenMail(JIRAServerSetup.SMTP);
		} catch (BindException e) {
			fail("Error configuring and starting greenmail server");
		}
    	
    	assertTrue("Mail server not running.", mailService.isRunning());
    	
    	navigation.gotoResource("OutgoingMailServers.jspa");
    	
    	if (tester.getDialog().isLinkPresent("deleteSMTP")) {
            tester.clickLink("deleteSMTP");
            tester.submit("Delete");
        }
    	
        tester.clickLinkWithText("Configure new SMTP mail server");
        tester.setFormElement("name", "Local Test Server");
        tester.setFormElement("from", EmailFuncTestCase.DEAFULT_FROM_ADDRESS);
        tester.setFormElement("prefix", EmailFuncTestCase.DEFAULT_SUBJECT_PREFIX);
        tester.setFormElement("serverName", "localhost");
        
        String port = String.valueOf(mailService.getSmtpPort());
        log("Setting SMTP server to 'localhost:" + port + "'");
        tester.setFormElement("port", port);
        tester.submit("Add");
        tester.assertLinkNotPresentWithText("Configure new SMTP mail server");
        tester.assertTextPresent("Local Test Server");
	}
    
    protected WebForm getFormByName(String formName){
    	WebForm[] forms = form.getForms();
    	WebForm watcherForm = null;
    	
    	for(WebForm form : forms){
    		log.log(form.getName());
    		if(form.getName().equals(formName))
    			return form;
    	}
    	
    	if(watcherForm == null)
    		fail("No form found with name " + formName);
    	
    	return null;
    }
    
    protected void gotoProjectParentWatchers(String project){
    	administration.project().viewProject(project);
    	navigation.clickLinkWithExactText(Parent_WatcherS_LINK);
    }
    
    @Override
	protected void setUpTest() {
		super.setUpTest();
	}
    
    /**
     * Tests accessing the Parent Watchers page via the "Edit Parent Watchers" link
     * @throws FileNotFoundException 
     */
    public void testAccessingParentWatchersPage() {
    	administration.restoreData("WithoutParentWatchers.zip");
    	
    	administration.project().viewProject(PROJECT_NAME);
    	tester.assertLinkPresentWithText(Parent_WatcherS_LINK);
    	navigation.clickLinkWithExactText(Parent_WatcherS_LINK);
    	assertions.getTextAssertions().assertTextPresent("JIRA Parent Watcher - Test");
    }

    /**
     * Tests adding users and groups as watchers to a component.
     * @throws SAXException 
     * @throws IOException 
     */
    public void testAddingParentWatchers() throws IOException, SAXException {
    	administration.restoreData("WithoutParentWatchers.zip");
    	
    	gotoProjectParentWatchers(PROJECT_NAME);
    	
    	// Test adding users
    	tester.clickLink("editUserWatchers_" + COMPONENT_ART_ID);
    	assertions.getTextAssertions().assertTextPresent("Add Users");
    	tester.getDialog().setFormParameter("watcherField", ADMIN_USERNAME);
    	tester.getDialog().submit("add_watchers");

    	assertions.getTextAssertions().assertTextPresent(ADMIN_FULLNAME + " (" + ADMIN_USERNAME + ")");
    	tester.assertFormElementPresent("removeWatcher_" + ADMIN_USERNAME);

    	tester.clickLink("return_link");
    	tester.assertElementPresent("component_" + COMPONENT_ART_ID + "_user_watcher_" + ADMIN_USERNAME);
    	
    	// Test adding groups
    	tester.clickLink("editGroupWatchers_" + COMPONENT_CODE_ID);
    	assertions.getTextAssertions().assertTextPresent("Add Groups");
    	form.selectOptionsByValue("groupWatchers[]", new String[]{JIRA_DEV_GROUP, JIRA_ADMIN_GROUP});
    	tester.getDialog().submit("add_watchers");
    	
    	assertions.getTextAssertions().assertTextPresent(JIRA_DEV_GROUP);
    	tester.assertFormElementPresent("removeWatcher_" + JIRA_DEV_GROUP);
    	assertions.getTextAssertions().assertTextPresent(JIRA_ADMIN_GROUP);
    	tester.assertFormElementPresent("removeWatcher_" + JIRA_ADMIN_GROUP);
    	tester.clickLink("return_link");
    	
    	tester.assertElementPresent("component_" + COMPONENT_CODE_ID + "_group_watcher_" + JIRA_DEV_GROUP);
    	tester.assertElementPresent("component_" + COMPONENT_CODE_ID + "_group_watcher_" + JIRA_ADMIN_GROUP);
    }
    
    public void testAddingDuplicateParentWatchers(){
    	administration.restoreData("WithParentWatchers.zip");

    	gotoProjectParentWatchers(PROJECT_NAME);

    	// Test adding users with a user already added
    	tester.clickLink("editUserWatchers_" + COMPONENT_ART_ID);
    	tester.getDialog().setFormParameter("watcherField", ADMIN_USERNAME + "," + BOB_USERNAME);
    	tester.getDialog().submit("add_watchers");

    	assertions.getTextAssertions().assertTextPresent("&quot;" + ADMIN_USERNAME + "&quot; is already a watcher on this component.");
    	
    	// Make sure the other user was adding along side the duplicate user
    	assertions.getTextAssertions().assertTextPresent(ADMIN_FULLNAME + " (" + ADMIN_USERNAME + ")");
    	tester.assertFormElementPresent("removeWatcher_" + ADMIN_USERNAME);
    	assertions.getTextAssertions().assertTextPresent(BOB_FULLNAME + " (" + BOB_USERNAME + ")");
    	tester.assertFormElementPresent("removeWatcher_" + BOB_USERNAME);

    	tester.clickLink("return_link");
    	
    	tester.assertElementPresent("component_" + COMPONENT_ART_ID + "_user_watcher_" + ADMIN_USERNAME);
    	tester.assertElementPresent("component_" + COMPONENT_ART_ID + "_user_watcher_" + BOB_USERNAME);
    }
    
    public void testEnableParentWatchers(){
		administration.restoreData("WithoutParentWatchers.zip");
		
    	assertTrue(administration.plugins().isPluginEnabled(PLUGIN_KEY));
    	administration.plugins().disablePlugin(PLUGIN_KEY);
    	assertTrue(administration.plugins().isPluginDisabled(PLUGIN_KEY));
    }
    
    /**
     * Tests the notifications with notification type specified 
     * @throws BindException 
     * @throws InterruptedException 
     * @throws IOException 
     * @throws MessagingException 
     */
    public void testNotificationsWithNotificationType() throws InterruptedException, MessagingException, IOException {
    	administration.restoreData("WithParentWatchers.zip");
    	
    	configureAndStartSmtpServer();
    	
    	// Made user a Parent Watcher
    	gotoProjectParentWatchers(PROJECT_NAME);
    	tester.clickLink("editUserWatchers_" + COMPONENT_OTHER_ID);
    	tester.getDialog().setFormParameter("watcherField", BOB_USERNAME);
    	tester.getDialog().submit("add_watchers");

    	// Update the notification scheme
    	navigation.gotoResource("ViewNotificationSchemes.jspa");
    	tester.clickLinkWithText("Default Notification Scheme");
    	tester.clickLink("add_1");
    	
    	// Verify that the Parent Watcher notification type exists
    	tester.assertRadioOptionValuePresent("type", "Parent_Watchers");
    	tester.checkRadioOption("type", "Parent_Watchers");
    	tester.submit("Add");
    	
    	// Create issue that should be received by two users
    	String subject = "Test issue with notification set";
    	Map<String, String[]> params = new HashMap<String, String[]>();
    	params.put("components", new String[]{String.valueOf(COMPONENT_OTHER_ID)});
    	navigation.issue().createIssue(PROJECT_NAME, ISSUE_TYPE_BUG, subject, params);
    	
    	flushMailQueueAndWait(2);
    	
    	// Check that only email was sent to admin and bob
    	assertCorrectNumberEmailsSent(2);
    	ArrayList<String> recipients = new ArrayList<String>();
    	recipients.add(ADMIN_EMAIL);
    	recipients.add(BOB_EMAIL);
    	assertRecipientsHaveMessages(recipients);
    }
    
    /**
     * Test removing all users and groups using the "All" checkbox
     * 
     * TODO: Uses javascript to select all watcher.  Find a way to do integration test w/ javascript.
     */
    /*
    public void testRemoveAllCheckbox() {
    	testAddingParentWatchers();
    	WebLink[] links = page.getLinksWithExactText("Edit");
    	log.log("Found "+links.length+" 'Edit' links.");

    	for(WebLink link : links){
    		log.log("Clicking link: "+link.asText());
    		navigation.clickLink(link);
    		tester.checkCheckbox("removeAllWatchers");
    		tester.getDialog().submit("remove_watchers");

    		if(link.getID().startsWith("editUserWatchers_")){
    			log.log("editUserWatchers");
        		assertFalse("Admin still found after deletion", tester.getDialog().hasFormParameterNamed("removeWatcher_"+ADMIN));
        		assertFalse("Bob still found after deletion", tester.getDialog().hasFormParameterNamed("removeWatcher_"+USER_BOB));
    		}else if(link.getID().startsWith("editGroupWatchers_")){
    			log.log("editGroupWatchers");
    			assertFalse("jira-users still found after deletion", tester.getDialog().hasFormParameterNamed("removeWatcher_jira-users"));
    			assertFalse("jira-developers still found after deletion", tester.getDialog().hasFormParameterNamed("removeWatcher_jira-developers"));
    		}
    		
    		tester.clickLink("return_link");    		
    	}       
    }*/

    /**
     * Tests the notifications without notification type specifid
     * @throws BindException 
     * @throws InterruptedException 
     * @throws IOException 
     * @throws MessagingException 
     */
    public void testNotificationsWithoutNotificationType() throws InterruptedException, MessagingException, IOException {
    	administration.restoreData("WithParentWatchers.zip");
    	
    	configureAndStartSmtpServer();
    	
    	// Made user a Parent Watcher
    	gotoProjectParentWatchers(PROJECT_NAME);
    	tester.clickLink("editUserWatchers_" + COMPONENT_OTHER_ID);
    	tester.getDialog().setFormParameter("watcherField", BOB_USERNAME);
    	tester.getDialog().submit("add_watchers");
    	
    	// Create issue that should only be received by one user
    	String subject = "Test issue without notification set";
    	Map<String, String[]> params = new HashMap<String, String[]>();
    	params.put("components", new String[]{String.valueOf(COMPONENT_OTHER_ID)});
    	navigation.issue().createIssue(PROJECT_NAME, ISSUE_TYPE_BUG, subject, params);
    	
    	flushMailQueueAndWait(1);
    	
    	// Check that only email was sent to admin
    	assertCorrectNumberEmailsSent(1);
    	ArrayList<String> recipients = new ArrayList<String>();
    	recipients.add(ADMIN_EMAIL);
    	assertRecipientsHaveMessages(recipients);
    }

    /**
     * Tests removing a user and group as watchers to a component.
     * @throws SAXException 
     * @throws IOException 
     */
    public void testRemovingParentWatchers() throws IOException, SAXException {
    	administration.restoreData("WithParentWatchers.zip");
    	
    	gotoProjectParentWatchers(PROJECT_NAME);
    	
    	tester.clickLink("editUserWatchers_" + COMPONENT_ART_ID);
    	
    	WebForm watcherForm = getFormByName("deleteWatchersForm");
    	watcherForm.setCheckbox("removeWatcher_" + ADMIN_USERNAME, true);
    	watcherForm.submit();
    	
    	tester.clickLink("return_link");
    	tester.assertElementNotPresent("component_" + COMPONENT_ART_ID + "_user_watcher_" + ADMIN_USERNAME);
    	
    	tester.clickLink("editGroupWatchers_" + COMPONENT_CODE_ID);
    	
    	watcherForm = getFormByName("deleteWatchersForm");
    	watcherForm.setCheckbox("removeWatcher_" + JIRA_ADMIN_GROUP, true);
    	watcherForm.submit();
    	
    	tester.clickLink("return_link");
    	
    	tester.assertElementPresent("component_" + COMPONENT_CODE_ID + "_group_watcher_" + JIRA_DEV_GROUP);
    	tester.assertElementNotPresent("component_" + COMPONENT_CODE_ID + "_group_watcher_" + JIRA_ADMIN_GROUP);
    }
}