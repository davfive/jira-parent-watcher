package it.com.davfive.jira;

public class IntegrationTestHelper {
	public static final String PLUGIN_KEY = "com.davfive.jira.plugin.jiraParentWatcher";
    
    public static String PROJECT_KEY = "TST";
    public static String PROJECT_NAME = "Test";
    public static int PROJECT_ID = 10000;
    
    public static String COMPONENT_CODE_NAME = "Code";
    public static int COMPONENT_CODE_ID = 10000;
    
    public static String COMPONENT_ART_NAME = "Art";
    public static int COMPONENT_ART_ID = 10001;
    
    public static String COMPONENT_DESIGN_NAME = "Design";
    public static int COMPONENT_DESIGN_ID = 10002;
    
    public static String COMPONENT_OTHER_NAME = "Other";
    public static int COMPONENT_OTHER_ID = 10003;
    
    public static String Parent_WatcherS_LINK = "Edit Parent Watchers";
}
