package com.davfive.jira.plugin;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.notification.NotificationType;
import com.atlassian.jira.notification.NotificationTypeManager;
import com.davfive.jira.notification.type.ParentWatcherNotificationType;

public class ParentWatcherLifecycle implements InitializingBean, DisposableBean{
	private static final Logger log = LoggerFactory.getLogger(ParentWatcherLifecycle.class);

	@Override
	public void destroy() throws Exception {
		log.debug("Disabling Parent Watcher Plugin");
		NotificationTypeManager notificationTypeManager = ComponentAccessor.getComponent(NotificationTypeManager.class);
		Map<String, NotificationType> types = notificationTypeManager.getTypes();

		String notificationLabel = ParentWatcherNotificationType.getLabel();
		log.debug("Using notification type label for the Parent Watcher: " + notificationLabel);
		
		if(types.containsKey(notificationLabel)) {
			log.debug("Found " + notificationLabel + " as a notification type.  Removing.");
			types.remove(notificationLabel);
			notificationTypeManager.setSchemeTypes(types);
		}
		
		log.debug("Parent Watcher Plugin disabled.");
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		log.debug("Enabling Parent Watcher Plugin");
		
		NotificationTypeManager notificationTypeManager = ComponentAccessor.getComponent(NotificationTypeManager.class);
		Map<String, NotificationType> types = notificationTypeManager.getTypes();
		
		String notificationLabel = ParentWatcherNotificationType.getLabel();
		log.debug("Using notification type label for the Parent Watcher: " + notificationLabel);
		
		if(!types.containsKey(notificationLabel)) {
			log.debug("Did not find " + notificationLabel + " as a notification type.  Adding.");
			types.put(notificationLabel, new ParentWatcherNotificationType());
			notificationTypeManager.setSchemeTypes(types);
		}
		
		log.debug("Parent Watcher Plugin enabled.");
	}
}
