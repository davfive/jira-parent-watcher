package com.davfive.jira.notification;

import java.util.Map;

import com.atlassian.jira.notification.NotificationType;
import com.atlassian.jira.notification.NotificationTypeManager;
import com.davfive.jira.notification.type.ParentWatcherNotificationType;

public class ParentWatcherNotificationTypeManager extends NotificationTypeManager {
	@Override
	public void setSchemeTypes(Map<String, NotificationType> schemeType) {
		schemeType.put("Parent_Watchers", new ParentWatcherNotificationType());
		super.setSchemeTypes(schemeType);
	}
}
